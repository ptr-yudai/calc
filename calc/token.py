# coding: utf-8

""" 字句解析器 """
class Tokenizer(object):
    def __init__(self, source):
        """ 初期化 """
        self.source = source
        self.index = 0
        self.token = None

    def empty(self):
        """ 全て読み取ったか """
        return len(self.source) - 1 < self.index

    def next_char(self):
        """ 次の文字を取り出す """
        if self.empty():
            return None
        c = self.source[self.index]
        self.index += 1
        return c

    def peek_char(self):
        """ 次の文字を先読みする """
        if self.empty():
            return None
        return self.source[self.index]

    def next_token(self):
        """ 次の字句を取り出す """
        self.token = self.next_char()
        if self.token is None:
            return None
        # 空白を飛ばす
        while self.token == ' ':
            self.token = self.next_char()
        if self.is_digit(self.token):
            # 数字
            if not self.empty():
                point = False
                c = self.next_char()
                while self.is_digit(c):
                    if c == '.':
                        # 小数点
                        if point:
                            raise TokenizerException(
                                "Invalid token near '{0}' at {1}".format(
                                    self.token + c, self.index
                                )
                            )
                        else:
                            point = True
                    self.token += c
                    if self.empty():
                        break
                    c = self.next_char()
                if not self.empty() and not self.is_operator(c):
                    raise TokenizerException(
                        "Invalid token near '{0}' at {1}".format(
                            self.token + c, self.index
                        )
                    )
                self.index -= 1
        elif self.is_variable(self.token):
            # レジスタ
            pass
        elif self.token == '<':
            # 左シフト
            if self.peek_char() == '<':
                self.token += self.next_token()
        elif self.token == '>':
            # 右シフト
            if self.peek_char() == '>':
                self.token += self.next_token()
        elif self.token == '/':
            # 切り捨て除算
            if self.peek_char() == '/':
                self.token += self.next_token()
        elif self.is_operator(self.token):
            # 演算子
            pass
        elif self.token in '()':
            # 括弧
            pass
        else:
            raise TokenizerException(
                "Invalid token near '{0}' at {1}".format(
                    self.token, self.index
                )
            )
        return self.token

    def peek_token(self):
        """ 次の字句を先読みする """
        index = self.index
        token = self.next_token()
        if token is not None:
            self.index = index
        return token
            
    def is_digit(self, c):
        """ 数字かどうか """
        return c in '0123456789.'

    def is_variable(self, c):
        """ 変数かどうか """
        return c == 'x'
        
    def is_operator(self, c):
        """ 演算子かどうか """
        return c in ['+', '-', '/', '*', '^', '%', '<<', '>>', '//', ' ']

class TokenizerException(Exception):
    pass
