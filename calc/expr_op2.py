# coding: utf-8
from expr import Expr

""" 2項演算子の構文木 """
class ExprOp2(Expr):
    """ 初期化 """
    def __init__(self, op, left, right):
        super(ExprOp2, self).__init__()
        self.op = op
        self.left = left
        self.right = right
    
    """ 評価 """
    def evaluate(self):
        return self.op(self.left.evaluate(), self.right.evaluate())
