# coding: utf-8
from expr import Expr

""" 単項演算子の構文木 """
class ExprOp1(Expr):
    """ 初期化 """
    def __init__(self, op, value):
        super(ExprOp1, self).__init__()
        self.op = op
        self.value = value
    
    """ 評価 """
    def evaluate(self):
        return self.op(self.value.evaluate())
