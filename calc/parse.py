# coding: utf-8
from token import Tokenizer
from expr_val import ExprValue
from expr_op1 import ExprOp1
from expr_op2 import ExprOp2
from reg import Register
from ope import *

""" パーサ """
class Parser(object):
    """
    C  -> SC
    C' -> %SC' | e
    S  -> ES
    S' -> <<ES' | >>ES' | e
    E  -> TE
    E' -> +TE' | -TE' | e
    T  -> FT
    T' -> *FT' | /FT' | //FT' | e
    F  -> F' | F'^F
    F' -> (C) | +N | -N | N
    N  -> 0123456789 | x
    """
    def __init__(self, expr, reg):
        """ 初期化 """
        self.tokenizer = Tokenizer(expr)
        self.reg = reg

    def parse(self):
        """ パース """
        self.tokenizer.next_token()
        return self.cluster1()

    def cluster1(self):
        """ C -> SC """
        sector = self.sector1()
        return self.cluster2(sector)

    def cluster2(self, expr1):
        """ C -> %SC' | e """
        if self.tokenizer.token == '%':
            self.tokenizer.next_token()
            expr2 = self.sector1()
            return self.cluster2(ExprOp2(op_mod, expr1, expr2))
        else:
            return expr1

    def sector1(self):
        """ S -> ES """
        expr = self.expr1()
        return self.sector2(expr)

    def sector2(self, expr1):
        """ S' -> <<ES' | >>ES' | e """
        if self.tokenizer.token == '<<':
            self.tokenizer.next_token()
            expr2 = self.expr1()
            return self.sector2(ExprOp2(op_shl, expr1, expr2))
        elif self.tokenizer.token == '>>':
            self.tokenizer.next_token()
            expr2 = self.expr1()
            return self.sector2(ExprOp2(op_shr, expr1, expr2))
        else:
            return expr1

    def expr1(self):
        """ E  -> TE """
        expr = self.term1()
        return self.expr2(expr)

    def expr2(self, expr1):
        """ E' -> +TE' | -TE' | e """
        if self.tokenizer.token == '+':
            self.tokenizer.next_token()
            expr2 = self.term1()
            return self.expr2(ExprOp2(op_add, expr1, expr2))
        elif self.tokenizer.token == '-':
            self.tokenizer.next_token()
            expr2 = self.term1()
            return self.expr2(ExprOp2(op_sub, expr1, expr2))
        else:
            return expr1

    def term1(self):
        """ T  -> FT """
        expr = self.factor1()
        return self.term2(expr)

    def term2(self, expr1):
        """ T' -> *FT' | /FT' | //FT' | e """
        if self.tokenizer.token == '*':
            self.tokenizer.next_token()
            expr2 = self.factor1()
            return self.term2(ExprOp2(op_mul, expr1, expr2))
        elif self.tokenizer.token == '/':
            self.tokenizer.next_token()
            expr2 = self.factor1()
            return self.term2(ExprOp2(op_div, expr1, expr2))
        elif self.tokenizer.token == '//':
            self.tokenizer.next_token()
            expr2 = self.factor1()
            return self.term2(ExprOp2(op_ddiv, expr1, expr2))
        else:
            return expr1

    def factor1(self):
        """ F  -> F'|F'^F """
        expr1 = self.factor2()
        if self.tokenizer.token == '^':
            self.tokenizer.next_token()
            expr2 = self.factor1()
            return ExprOp2(op_pow, expr1, expr2)
        else:
            return expr1

    def factor2(self):
        """ F' -> (C) | +N | -N | N """
        if self.tokenizer.is_digit(self.tokenizer.token[0]):
            return self.value()
        elif self.tokenizer.is_variable(self.tokenizer.token[0]):
            return self.variable()
        elif self.tokenizer.token == '+':
            self.tokenizer.next_token()
            return self.value()
        elif self.tokenizer.token == '-':
            self.tokenizer.next_token()
            return ExprOp1(op_neg, self.value())
        elif self.tokenizer.token == '(':
            self.tokenizer.next_token()
            expr = self.cluster1()
            self.tokenizer.next_token()
            return expr
        else:
            raise ParserException("Invalid expression.")

    def value(self):
        """ N -> Value """
        expr = ExprValue(float(self.tokenizer.token))
        self.tokenizer.next_token()
        return expr

    def variable(self):
        """ N -> Variable """
        expr = Register(self.reg)
        self.tokenizer.next_token()
        return expr

class ParserException(Exception):
    pass
