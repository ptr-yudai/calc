# coding: utf-8
from expr import Expr

""" レジスタ """
class Register(Expr):
    def __init__(self, reg):
        """ 初期化 """
        self.reg = reg

    def evaluate(self):
        """ 評価 """
        return self.reg
