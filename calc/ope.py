# coding: utf-8

""" 演算子 """
op_add = lambda x, y: x + y
op_sub = lambda x, y: x - y
op_mul = lambda x, y: x * y
op_div = lambda x, y: x / y
op_ddiv= lambda x, y: x // y
op_mod = lambda x, y: x % y
op_pow = lambda x, y: x ** y
op_shl = lambda x, y: int(x) << int(y)
op_shr = lambda x, y: int(x) >> int(y)
op_neg = lambda x: -x
