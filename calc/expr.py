# coding: utf-8
""" 構文木の実装 """
class Expr(object):
    """ 初期化 """
    def __init__(self):
        pass

    """ 評価 """
    def evaluate():
        raise NotImplementedError()
