# coding: utf-8
from expr import Expr

""" 数値の構文木 """
class ExprValue(Expr):
    """ 初期化 """
    def __init__(self, value):
        super(ExprValue, self).__init__()
        self.value = value
    
    """ 評価 """
    def evaluate(self):
        return self.value
