#!/usr/bin/env python
# coding: utf-8
import sys
import calc

if __name__ == '__main__':
    print("***** calc v0.1 *****")
    x = new_x = 0.0
    while True:
        sys.stdout.write(">> ")
        f = sys.stdin.readline().strip()
        # パース
        parser = calc.parse.Parser(f, x)
        try:
            ast = parser.parse()
        except calc.token.TokenizerException, e:
            print("[ERROR] TokenizerException")
            print(e)
            continue
        except calc.parse.ParserException, e:
            print("[ERROR] ParserException")
            print(e)
            continue
        except Exception, e:
            print("[FATAL] Exception")
            print(e)
            continue
        # 評価
        try:
            new_x = ast.evaluate()
            print(new_x)
        except Exception, e:
            print("[FATAL] Exception")
            print(e)
            continue
        # 更新
        x = new_x
